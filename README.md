# NÖGIS Machbarkeitsstudie OpenLayers Mobile Offline

### Inhaltsverzeichnis

 * [Installation](#markdown-header-installation)
 * [Dokumentation](#markdown-header-dokumentation)
     * [Allgemeines zu Offline Applikationen](#markdown-header-allgemeines-zu-offline-applikationen)
     * [Aufbereitung von Rasterdaten](#markdown-header-aufbereitung-von-vektordaten)
     * [Aufbereitung von Rasterdaten](#markdown-header-aufbereitung-von-rasterdaten)
     * [Arbeiten mit Offline-Kacheln in OpenLayers](#markdown-header-arbeiten-mit-offline-kacheln-in-openlayers)
     * [Erfassung geographischer Merkmale](#markdown-header-erfassung-geographischer-merkmale)
     * [Synchronisierung](#markdown-header-synchronisierung)
 * [API](./src/readme.md)

## Installation

### Voraussetzungen

Dieses Projekt benötigt Node.js in der aktuellen LTS Version. Download von https://nodejs.org.

### Erhalten des Codes

Entweder durch Download und Entpacken des ZIP von https://bitbucket.org/ahocevar/noegis-mobile/downloads, oder durch Klonen des Repository:

    git clone git@bitbucket.org:ahocevar/noegis-mobile.git

### Installation

Installation der Abhängigkeiten:

    cd noegis-mobile
    npm install

Zur Aufbereitung der Kacheln sind [tippecanoe](https://github.com/mapbox/tippecanoe),  [gdal](http://www.gdal.org) und [rasterio](https://github.com/mapbox/rasterio) erforderlich. Die Installation dieser Utilities erfolgt nach Anleitung des jeweiligen Anbieters manuell. Nach der Installation werden die Beispieldaten durch Ausführen von

    npm run data

aufbereitet. **Hinweis** Dieser Task funktioniert nur auf Unix-Systemen (z.B. Linux, MacOS) automatisch. Unter Windows muss nach der Anleitung zum Aufbereiten von Vektor- und Rasterdaten in der [Dokumentation](#markdown-header-dokumentation) vorgegangen werden.

### Ausführen des Codes

Starten des Debug-Servers

    npm run serve-examples -- --ssl

Den Anweisungen im Terminal folgen, um die Startseite im Browser zu öffnen.

### Deployment

Komprimierten Programmcode zum Hochladen auf einen Server erzeugen:

    npm run build-examples

Der komplette Inhalt des `build/`-Verzeichnisses wird sodann auf den Production Server kopiert.

## Dokumentation

### Allgemeines zu Offline Applikationen

Die zur Verfügung stehenden Browser-Technologien unterliegen derzeit noch einer relativ starken Fluktuation, daher gibt es Unterschiede je nach Browser und Browser-Version. Prinzipiell empfielt sich der Einsatz von aktuellen Chrome oder Edge Versionen, bzw. Safari auf iOS. Safari wurde im Rahmen dieser Machbarkeitsstudie nicht intensiv getestet, da es von Auftraggeberseite dazu keine hohe Priorität gab.

Wie eine vollständig offline funktionierende Applikation konfiguriert ist, zeigt das Beispiel `tile-store.html` mit dem Cache Manifest `tile-store.appcache`.

Prinzipiell brauchen wir für eine komplett offline laufende Applikation zwei Speicherebenen: Applikation und Daten.

#### Offline Applikation

Die Applikation selbst muss offline verfügbar sein. Dafür steht derzeit ausschließlich die AppCache Technologie zur Verfügung, welche jedoch bereits als deprecated markiert ist und mittelfristig in aktuellen Browsern nicht mehr unterstützt werden wird. Als Alternative können künftig bis zu einem gewissen Grad Service Workers verwendet werden. Diese Technologie steht derzeit noch nicht in allen Browsern zur Verfügung, und zu deren Einsatz braucht man zumindest eine als Data URI speicherbare HTML-Seite mit etwas JavaScript, welche die Basisumgebung bereitstellt, um den Rest der Applikation über Service Workers aus einem Offline Speicher (z.B. IndexedDB) zu laden.

#### Offline Daten

Die Daten, die von der Applikation benötigt werden, müssen offline gespeichert werden. Dazu kommt vorzugsweise IndexedDB zum Einsatz.

#### Speicherlimits

Verglichen mit älteren Technologien (z.B. LocalStorage) steht mit IndexedDB sehr viel Speicher zur Verfügung. Die Limits hängen vom verwendeten Browser und vom insgesamt verfügbaren Speicher auf der Partition, die das Benutzerprofil enthält, ab. Auf den getesteten Endgeräten waren in Chrome zwischen 700 MB (Samsung) und 7 GB (Getac) verfügbar.

Es empfiehlt sich nicht, mehr als 1 GB offline zu speichern. IndexedDB Lese- und Schreibzugriffe werden mit zunehmender Anzahl der gespeicherten Elemente deutlich langsamer. Anstatt z.B. Kacheln für ganz Niederösterreich offline verfügbar zu machen, empfiehlt es sich eher, nur das unmittelbar benötigte Bearbeitungsgebiet zwischen zwei Innendiensten zu speichern. Derartige Seed-Vorgänge sind meist auch in wenigen Minuten erledigt und sollten daher leicht in den Arbeitsablauf integrierbar sein.

IndexedDB ist eine Empfehlung des W3C, wird als Nachfolger von WebSQL und LocalStorage gehandelt und wird nach wie vor weiterentwickelt. Der Support ist in allen modernen Browsern gegeben. Standardmäßig ist der Storage-Typ "best effort", wenn das System den Platz benötigt, können zufällige Datenbanken ohne Wissen des Nutzers gelöscht werden. Echte Persistenz kann in den Browsern Chrome 55, Firefox 57 und Opera 42 erreicht werden ([-> aktueller Browsersupport](https://developer.mozilla.org/en-US/docs/Web/API/StorageManager/persist)). In den Modulen dieser Machbarkeitsstudie wird Persistenz für editierbare Vektordaten verwendet.

#### Sonstige Einschränkungen

Zuverlässige Funktion von Offline-Applikationen ist nur gewährleistet, wenn ausschließlich https mit gültigen SSL-Zertifikaten verwendet wird.

#### Empfohlene Browser

In unseren Tests hat sich Chrome als zuverlässigster Browser beim Arbeiten mit IndexedDB herausgestellt. Firefox und Safari haben mehrere bekannte Fehler im Zusammenhang mit IndexedDB. Firefox kündigt für Version 60 wesentliche Verbesserungen an. Edge funktioniert gut, lediglich das Löschen von Datenbanken funktioniert programmatisch nicht und muss über die Einstellungen durchgeführt werden.

### Aufbereitung von Vektordaten

Für Vektordaten, die nicht editiert werden, bietet sich die Verwendung von Vektorkacheln an. Diese können komfortabel mit [tippecanoe](https://github.com/mapbox/tippecanoe) aus GeoJSON Dateien erzeugt werden.

**Wichtig**: Um die erzeugten Kacheln sofort auf einem statischen Webserver nutzen zu können, müssen in `tippecanoe` die Optionen `--no-tile-compression` und `--output-to-directory` verwendet werden.

Als Beispiel sei die Umwandlung der [OGD Widmungsumhüllenden](https://www.data.gv.at/katalog/dataset/81a23612-9823-4a61-95cb-09aea8242db8) des Landes Niederösterreich von GeoJSON nach Mapbox Vector Tiles angeführt:

    tippecanoe --no-tile-compression --minimum-zoom=10 --maximum-zoom=15 --detect-shared-borders widmung.json --output-to-directory=widmung -f


### Aufbereitung von Rasterdaten

Zum Zerteilen von Rasterdaten im TIFF, GeoTIFF oder PNG Format kann `gdal2tiles.py` verwendet werden. In Tests erwies sich dieses Skript bei sehr großen Datenmengen in Kombination mit externen Festplatten als nicht allzu stabil, aber nach mehreren Anläufen konnte auch ein 1 m DEM für ganz Niederösterreich bis Zoomstufe 17 erzeugt werden.

Nachfolgend wird die Vorgehensweise anhand des OGD Höhenmodells von Niederösterreich (10 m Raster) mit einem Band, welches die Höhe in m enthält, beschrieben. Dieses wird von [noe.gv.at](http://www.noe.gv.at/noe/OGD_Detailseite.html?id=46a7a06a-f69b-405e-aac2-77f775449ad3) heruntergeladen. Danach kommt zum Umwandeln des Höhenbandes in browsertaugliche RGB Bänder das im Rahmen der Machbarkeitsstudie erstellte Python-Skript [dem2rgb.py](./dem2rgb.py) zum Einsatz, welches [rasterio](https://github.com/mapbox/rasterio) benötigt. Die komplette Sequenz zur Aufbereitung der Daten ist folgende:

    python dem2rgb.py DTM_10x10.tif dtm_10x10.rgb.tif
    gdal2tiles.py -z8-14 dtm_10x10.rgb.tif dtm_10x10

Das Ergebnis ist ein png-Kachelset mit den Zoomstufen 8-14. Die Höheninformation ist in die rgb-Werte kodiert, und kann wie folgt in die Höhe in m rückkonvertiert werden:

```js
const elevation = -10000 + (r * 256 * 256 + g * 256 + b) * 0.1;
```

Die Verwendung dieses Kachelsets zur Ableitung von Hangneigung und Geländeschummerung wird in diesem Beispiel demonstriert:

    elevation.html


### Arbeiten mit Offline-Kacheln in OpenLayers

Das im Rahmen der Machbarkeitsstudie erstellte Modul [TileLoader](./src/readme.md#markdown-header-tileloader) wird verwendet, um eine Tile Source in OpenLayers offline verfügbar zu machen. Das Modul stellt dann Funktionen zur Verfügung, um einen benutzerdefinierten Extent für einen benutzerdefinierten Zoombereich in der IndexedDB zu speichern.

Die Verwendung des TileLoader wird in diesem Beispiel demonstriert:

    tile-store.html


### Erfassung geographischer Merkmale

Nach ausführlicher Analyse sowohl bestehender Mobile- als auch Desktopapplikationen wurde eine plattformübergreifende Möglichkeit entwickelt, geographische Merkmale, insbesondere deren geometrische Ausprägungen, effizient im Feld zu erfassen. Diese äußert sich zum einen durch eine ergonomische Lösung für den Benutzer im Feld, zum anderen durch das plattformunabhängige permanente Speichern der Daten in IndexedDB.

Die Ergebnisse können nach der Installation, dem Deployment und dem Ausführen des Codes live in folgenden Examples getestet werden:

    loadLayers.html
    editing.html
    importExport.html


#### Erstellung eines Daten-Verwaltungssystems im Browser

Das Daten-Verwaltungssystem erfolgt ausschließlich in IndexedDB, die Datenbankkommunikation erfolgt entweder über nativen JavaScript-Code der IndexedDB-API oder über die populäre Bibliothek localForage.

Die durch die Applikation lokal erstellte Datenbank kann in den Developer Tools des Browsers inspiziert werden. In den Chrome Developer Tools ist die Datenkbank unter
  `Applikation -> Storage -> IndexedDB -> LayersDataBase`
ersichtlich. Um Veränderungen zu sehen muss diese manuell durch `Rechtsclick -> refresh IndexedDB` aktualisiert werden.

Mit IndexedDB können Key-Value-Paare in sogenannten "Object Stores" in selbst erstellten Datenbanken gespeichert werden. Der Key muss den Typ `string` aufweisen, als Value ist jeder JavaScript erlaubt. Für die geographischen Features wird als Key eine eindeutige ID generiert und als `string` verspeichert. Das Feature selbst stellt im GeoJSON-Format als `string` den value dar. Dieses Format stellt ein ausgezeichnetes Austauschformat für verschiedenste Desktop-GIS dar und erlaubt einen schnellen Import und Export aus IndexedDB. Die Performance der Kartenapplikation selbst wird durch das interne Format von OpenLayers nicht eingeschränkt.

Das Erstellen der Datenbank und das Laden vorhandener Layer (bzw. Beispiellayer) kann im Example `loadLayers.html` getestet werden.

#### Automatisches Erkennen der verwendeten Plattform (Touch, Maus)

Im Zuge dieser Machbarkeitsstudie wurden die Interaktionen auf mobilen Endgeräten (Finger & Stift) in OpenLayers verbessert (siehe https://github.com/openlayers/openlayers/pull/7703). Dadurch wird eine Differenzierung zwischen den verschiedenen Endgeräten obsolet.
Diese Veränderungen werden zukünftig in OpenLayers 5 standardmäßig erhältlich, sind jedoch bereits in der hier verwendeten Version außerhalb der offiziellen Releases implementiert.

Sollte eine Differenzierung zwischen Mobile und Desktop dennoch gewünscht werden, ist dies durch eine Vielzahl von kleinen Libraries möglich, zum Beisiel [mobile-detect](https://www.npmjs.com/package/mobile-detect). Selbst eine Unterscheidung zwischen Finger und Stift ist möglich, allerdings erst ab der ersten Berührung und nur in Browser, in denen die [PointerEvent-API](https://developer.mozilla.org/en-US/docs/Web/API/PointerEvent) implementiert wurde.

#### Datenerfassung

Die Datenerfassung sowie das Editieren der Daten geschieht offline über das Zeichnen verschiedener Geometrien (Polygon, Linie, Punkt) in der Karte. Um sowohl eine schnelle Datenaufnahme sowie eine möglichst genaue Punktsetzung zu gewährleisten, wurde ein kombinierte Lösung entwickelt. Bei einem kurzen Klick (bzw. Berührung) wird ein Punkt gesetzt.

Dauert der Klick bzw. die Berührung länger als 500ms, öffnet sich eine zweite Karte mit höherer Zoomstufe. Diese fungiert als Lupe, folgt dem Cursor mit einem Fadenkreuz und schließt sich automatisch wenn der Punkt durch den Release gesetzt wurde.

#### Editieren der Daten

Sollte die Lupenfunktion nicht für die gewünschte Genaugkeit ausreichen, ist ein späteres Modifizieren der Geometrie durch die OpenLayers-Interaktion `modify` möglich. Diese Interaktion ist im Example `editing.html` permanent möglich und wird durch einen langen Klick auf einen Punkt oder Eckpunkt aktiviert. Durch einen langen Klick auf eine Linie wird ein zusätzlicher Eckpunkt engefügt.

Die OpenLayers-Interaktion `Snapping` kann nach Bedarf aktiviert und deaktiviert werden.

Über die OpenLayers-Interaktion `Select` können einzelne Features selektiert werden. Bei einer Selektion öffnet sich seitlich ein Menüfenster, in diesem sind die ID sowie vorhandene Fotos ersichtlich und es können neue Fotos in das Feature eingefügt werden. Benützt man ein mobiles Endgerät, wird hierzu die Kamera geöffnet. Das Foto wird nach der Aufnahme über `FileReader.readAsDataURL()` in einen base64-codierten String umgewandelt. Diese Data-URIs werden als Array im GeoJSON gespeichert.

Jedes Feature weist einen eigenen Eintrag in IndexedDB auf. Dieser wird erstellt bzw. upgedated bei:

  * `drawend` eines Features
  * `modifyend` eines Features
  * hinzufügen eines Fotos

Weitere Attribute könnten, analog zu den Fotos, über die Properties des GeoJSON geladen, modifiziert und gespeichert werden. Das Erstellen und Editieren von Features sowie das hinzufügen von Fotos kann im Example `editing.html` getestet werden.

### Synchronisierung

Der Import von Dateien des GeoJSON-Formats erfolgt über ein HTML5 `Input FileUpload Object`. Die gewählte Datei wird gelesen und in IndexedDB als neuer Object Store gespeichert, wobei dessen Name der Name der geladenen Datei ist. Ist dieser Object Store Name bereits vergeben, wird der Benutzer um Erlaubnis gefragt, den alten Store zu überschreiben.

Für den Export wird das GeoJSON eines gewünschten Layers in ein `<a>` geschrieben und über das Download-Attribut und ein automatisch ausgelöstes Click-Event in das Standard-Downloadverzeichnis des Browsers gespeichert. Von dort kann es in jedes gängige Desktop-GIS importiert werden. Die Fotos, falls vorhanden, sind als Array von base-64-codierten Data-URIs im GeoJSON enthalten. Diese müssten nach nach einem Import in ein Desktop-GIS in Bilder zurück umgewandelt werden. Dies ist unter anderem in Python möglich.

Der Import und der Export von Daten können im Example `loadLayers.html` getestet werden.

Der Verbindungsstatus, ob eine Internetverbindung besteht, kann zuverlässig über JavaScript-Bibliotheken überprüft werden, z.B. über [is-online](https://www.npmjs.com/package/is-online)

Eine Möglichkeit, die aufgenommenen Daten online zu synchronisieren und zu verwalten ist [GeoGIG](http://geogig.org/workshop/workshop.html). GeoGIG ist ein von Git inspiriertes serverseitiges Versionsmanagementsystem mit Spezialisierung auf geographische Daten.
