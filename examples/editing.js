import 'ol/ol.css';
import Map from 'ol/CanvasMap';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import {fromLonLat} from 'ol/proj';
import LFModule from '../src/localForage.js';
import DrawFeature from '../src/DrawFeature.js';
import LoadSources from '../src/loadSources.js';


console.log(LFModule); //eslint-disable-line
console.log(LFModule.testModule); //eslint-disable-line
LFModule.init();

const map = new Map({
  target: 'map',
  moveTolerance: 3,
  layers: [
    new TileLayer({
      source: new XYZ({
        url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      })
    })
  ],
  view: new View({
    center: fromLonLat([15.4, 47]),
    zoom: 12
  })
});

//load all features from InitialStoreName
const loadSources = new LoadSources({
  map
});
loadSources.loadLayer('InitialStoreName', false);


// initialize interaction for main map
const selectionContainer = document.getElementById('chooseForm'); //html container for feature type selection
const zoomedMap = document.getElementById('zoomedMap');
const drawFeature = new DrawFeature({
  map,
  selectionContainer,
  zoomedMap
});

drawFeature.initInteractions(); // first time initialization (before change of selection)

selectionContainer.childNodes[0].onchange = function() { // after Form is initialized, add onchange interactions
  map.removeInteraction(drawFeature.draw);
  map.removeInteraction(drawFeature.snap);
  drawFeature.initInteractions();
};


// create second map after initializing first map, so the layers already exist
const magnifiedMap = new Map({
  target: 'zoomedMap',
  layers: map.getLayers().getArray(),
  interactions: [],
  controls: [],
  view: new View({
    //center: map.getView().getCenter(),
    zoom: map.getView().getZoom() + 2
  })
});
zoomedMap.style.display = 'none'; // cant use external css for some reason

document.getElementById('snappingButton').onclick = function() {
  let buttonText = document.getElementById('snappingButton').innerHTML;
  console.log('button text:', buttonText);
  if (buttonText === 'Snapping: OFF') {
    drawFeature.snapping(true);
    document.getElementById('snappingButton').innerHTML = 'Snapping: ON';
  } else {
    drawFeature.snapping(false);
    document.getElementById('snappingButton').innerHTML = 'Snapping: OFF';
  }
};

document.getElementById('selectingButton').onclick = function() {
  let buttonText = document.getElementById('selectingButton').innerHTML;
  console.log('button text:', buttonText);
  if (buttonText === 'Selecting: OFF') {
    drawFeature.selecting(true);
    document.getElementById('selectingButton').innerHTML = 'Selecting: ON';
  } else {
    drawFeature.selecting(false);
    document.getElementById('selectingButton').innerHTML = 'Selecting: OFF';
  }
};

//initializing drawFeature-module not possible because of shared vector source (would make 2 seperate vector sources)
//make additional interactions for the magnified map?

// syncing the two maps
let maps = [map, magnifiedMap];
let syncMaps = function(evt) {
  console.log('syncing maps');
  var type = evt.type.split(':');
  if (type[0] === 'change' && type.length === 2) {
    var attribute = type[1];
    var value = evt.target.get(attribute);
    if (attribute === 'resolution') {
      console.log('syncing zoom');
      if (map.getView().getZoom() !== magnifiedMap.getView().getZoom() + 2) {
        magnifiedMap.getView().setZoom(map.getView().getZoom() + 2);
      }
    } else if (magnifiedMap.getView().get(attribute) !== value) {
      magnifiedMap.getView().set(attribute, value);
    }
  } else {
    magnifiedMap.getView().setCenter(map.getEventCoordinate(evt));
  }
};

//map.getView().on('pointermove', syncMaps);
map.getTargetElement().addEventListener('pointermove', syncMaps);
map.getTargetElement().addEventListener('pointerdown', syncMaps);
map.getView().on('change:rotation', syncMaps);
map.getView().on('change:resolution', syncMaps);
