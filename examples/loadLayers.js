import 'ol/ol.css';
import Map from 'ol/CanvasMap';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import {fromLonLat} from 'ol/proj';
import LFModule from '../src/localForage.js';
import LoadSources from '../src/loadSources.js';

console.log(LFModule.testModule); //eslint-disable-line

LFModule.init();

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new XYZ({
        url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      })
    })
  ],
  view: new View({
    center: fromLonLat([15.4, 47]),
    zoom: 12
  })
});


const loadSources = new LoadSources({
  map
});


loadSources.getAllStoreNames().then((layerNames)=>{ // when all store names are retrieved, populate layer selection
  if (layerNames.length < 3) {loadSources.createRandomStores(layerNames.length) } // creating example stores
  const sourceSelectionContainer = document.getElementById('chooseForm'); //html container for feature type selection
  loadSources.initForm(sourceSelectionContainer, layerNames);

  sourceSelectionContainer.childNodes[0].onchange = function() { // after Form is initialized, add onchange interactions
    console.log(this.value);
    loadSources.loadLayer(this.value, true); //value of form
  };
});
