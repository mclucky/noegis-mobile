export function slope(inputs, data) {
  if (!this.elevation) {
    var functionData = data.elevation.match(/^function\s*([\w$]*)\s*\(([\w\s,$]*)\)\s*\{([\w\W\s\S]*)\}$/);
    this.elevation = Function.apply(null, functionData[2].split().concat(functionData[3]));
  }
  var elevation = this.elevation;
  var elevationImage = inputs[0];
  var width = elevationImage.width;
  var height = elevationImage.height;
  var elevationData = elevationImage.data;
  var slopeData = new Uint8ClampedArray(elevationData.length);
  var dp = data.resolution * 2;
  var maxX = width - 1;
  var maxY = height - 1;
  var pixel = [0, 0, 0, 0];
  var pixelX, pixelY, x0, x1, y0, y1, offset, z0, z1, dzdx, dzdy, slope;
  for (pixelY = 0; pixelY <= maxY; ++pixelY) {
    y0 = pixelY === 0 ? 0 : pixelY - 1;
    y1 = pixelY === maxY ? maxY : pixelY + 1;
    for (pixelX = 0; pixelX <= maxX; ++pixelX) {
      x0 = pixelX === 0 ? 0 : pixelX - 1;
      x1 = pixelX === maxX ? maxX : pixelX + 1;
      let nodata = false;

      // determine elevation for (x0, pixelY)
      offset = (pixelY * width + x0) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z0 = elevation(pixel);
      nodata = z0 == data.nodata;

      // determine elevation for (x1, pixelY)
      offset = (pixelY * width + x1) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z1 = elevation(pixel);
      nodata = nodata || z1 == data.nodata;

      dzdx = (z1 - z0) / dp;

      // determine elevation for (pixelX, y0)
      offset = (y0 * width + pixelX) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z0 = elevation(pixel);
      nodata = nodata || z0 == data.nodata;

      // determine elevation for (pixelX, y1)
      offset = (y1 * width + pixelX) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z1 = elevation(pixel);
      nodata = nodata || z1 == data.nodata;

      dzdy = (z1 - z0) / dp;

      slope = Math['atan'](Math['sqrt'](dzdx * dzdx + dzdy * dzdy));
      for (var t in data.colorMap) {
        if (slope < Number(t) / 100) {
          var color = data.colorMap[t];
          slopeData[offset] = color[0];
          slopeData[offset + 1] = color[1];
          slopeData[offset + 2] = color[2];
          break;
        }
      }
      offset = (pixelY * width + pixelX) * 4;
      slopeData[offset + 3] = nodata ? 0 : elevationData[offset + 3];
    }
  }

  return {data: slopeData, width: width, height: height};
}

export function shade(inputs, data) {
  if (!this.elevation) {
    var functionData = data.elevation.match(/^function\s*([\w$]*)\s*\(([\w\s,$]*)\)\s*\{([\w\W\s\S]*)\}$/);
    this.elevation = Function.apply(null, functionData[2].split().concat(functionData[3]));
  }
  var elevation = this.elevation;
  var elevationImage = inputs[0];
  var width = elevationImage.width;
  var height = elevationImage.height;
  var elevationData = elevationImage.data;
  var shadeData = new Uint8ClampedArray(elevationData.length);
  var dp = data.resolution * 2;
  var maxX = width - 1;
  var maxY = height - 1;
  var pixel = [0, 0, 0, 0];
  var twoPi = 2 * Math.PI;
  var halfPi = Math.PI / 2;
  var sunEl = Math.PI * data.sunEl / 180;
  var sunAz = Math.PI * data.sunAz / 180;
  var cosSunEl = Math.cos(sunEl);
  var sinSunEl = Math.sin(sunEl);
  var pixelX, pixelY, x0, x1, y0, y1, offset,
      z0, z1, dzdx, dzdy, slope, aspect, cosIncidence, scaled;
  for (pixelY = 0; pixelY <= maxY; ++pixelY) {
    y0 = pixelY === 0 ? 0 : pixelY - 1;
    y1 = pixelY === maxY ? maxY : pixelY + 1;
    for (pixelX = 0; pixelX <= maxX; ++pixelX) {
      x0 = pixelX === 0 ? 0 : pixelX - 1;
      x1 = pixelX === maxX ? maxX : pixelX + 1;
      let nodata = false;

      // determine elevation for (x0, pixelY)
      offset = (pixelY * width + x0) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z0 = data.vert * elevation(pixel);
      nodata = z0 == data.nodata * data.vert;

      // determine elevation for (x1, pixelY)
      offset = (pixelY * width + x1) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z1 = data.vert * elevation(pixel);
      nodata = nodata || z1 == data.nodata * data.vert;

      dzdx = (z1 - z0) / dp;

      // determine elevation for (pixelX, y0)
      offset = (y0 * width + pixelX) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z0 = data.vert * elevation(pixel);
      nodata = nodata || z0 == data.nodata * data.vert;

      // determine elevation for (pixelX, y1)
      offset = (y1 * width + pixelX) * 4;
      pixel[0] = elevationData[offset];
      pixel[1] = elevationData[offset + 1];
      pixel[2] = elevationData[offset + 2];
      pixel[3] = elevationData[offset + 3];
      z1 = data.vert * elevation(pixel);
      nodata = nodata || z1 == data.nodata * data.vert;

      dzdy = (z1 - z0) / dp;

      slope = Math['atan'](Math['sqrt'](dzdx * dzdx + dzdy * dzdy));

      aspect = Math['atan2'](dzdy, -dzdx);
      if (aspect < 0) {
        aspect = halfPi - aspect;
      } else if (aspect > halfPi) {
        aspect = twoPi - aspect + halfPi;
      } else {
        aspect = halfPi - aspect;
      }

      cosIncidence = sinSunEl * Math.cos(slope) +
          cosSunEl * Math.sin(slope) * Math.cos(sunAz - aspect);

      offset = (pixelY * width + pixelX) * 4;
      scaled = 255 * cosIncidence;
      shadeData[offset] = scaled;
      shadeData[offset + 1] = scaled;
      shadeData[offset + 2] = scaled;
      shadeData[offset + 3] = nodata ? 0 : elevationData[offset + 3];
    }
  }

  return {data: shadeData, width: width, height: height};
}
