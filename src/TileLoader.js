import idb from 'idb';
import {EventEmitter} from 'events';
import {getIntersection} from 'ol/extent';
import TileImage from 'ol/source/TileImage';
import VectorTile from 'ol/source/VectorTile';
import {loadFeaturesXhr} from 'ol/featureloader';
import {get as getProj} from 'ol/proj';


export default class TileLoader extends EventEmitter {

  constructor(options) {
    super();
    this.url = options.source.getTileUrlFunction();
    this.source = options.source;
    this.name = options.name;
    this.projection = getProj('EPSG:3857');
    this.pixelRatio = 1;
    this.applyLoader();
  }

  dbPromise() {
    return idb.open(this.name, 1, upgradeDb => {
      if (!upgradeDb.objectStoreNames.contains('tiles')) {
        upgradeDb.createObjectStore('tiles');
      }
    });
  }

  applyLoader() {
    this.dbPromise().then(db => {
      const source = this.source;
      this.source.setTileUrlFunction((tileCoord, pixelRatio, projection) => {
        this.pixelRatio = pixelRatio;
        this.projection = projection;
        return tileCoord.join(',');
      });
      this.source.setTileLoadFunction((tile, key) => {
        const tx = db.transaction('tiles', 'readonly');
        const tiles = tx.objectStore('tiles');
        if (source instanceof VectorTile) {
          tile.setLoader(() => {
            tiles.get(key).then(arrayBuffer => {
              if (!arrayBuffer) {
                throw new Error('not available');
              }
              console.log('vector tile offline: ' + this.name + ' ' + key); //eslint-disable-line
              const format = source.format_;
              tile.onLoad.call(tile, format.readFeatures(arrayBuffer, {featureProjection: this.projection}),
                  format.readProjection(source), format.getLastExtent());
            }).catch(() => {
              console.log('vector tile online: ' + this.name + ' ' + key); //eslint-disable-line
              const url = this.url(key.split(',').map(Number), this.pixelRatio, this.projection);
              loadFeaturesXhr(url, source.format_, tile.onLoad.bind(tile), tile.onError.bind(tile))(null, NaN, this.projection);
            });
          });
        } else if (source instanceof TileImage) {
          tiles.get(key).then(blob => {
            if (!blob) {
              throw new Error('not available');
            }
            console.log('image tile offline: ' + this.name + ' ' + key); //eslint-disable-line
            const url = URL.createObjectURL(blob);
            const image = tile.getImage();
            image.onload = function() {
              URL.revokeObjectURL(url);
            };
            image.src = url;
          }).catch(() => {
            console.log('image tile online: ' + this.name + ' ' + key); //eslint-disable-line
            const url = this.url(key.split(',').map(Number), this.pixelRatio, this.projection);
            tile.getImage().src = url;
          });
        }
      });
    });
  }

  deleteDatabase() {
    idb.delete(this.name).then(db => {
      console.log(this.name + '  deleted.'); //eslint-disable-line
    }).catch(err => {
      console.error('delete error: ' + err.message); //eslint-disable-line
    });
  }

  seed(options) {
    const source = this.source;
    const view = options.view;
    const seedExtent = options.extent || view.calculateExtent();
    const sourceExtent = source.getTileGrid().getExtent();
    if (sourceExtent) {
      getIntersection(seedExtent, sourceExtent, seedExtent);
    }
    let minZoom = options.minZoom;
    if (minZoom === undefined) {
      minZoom = Math.ceil(view.getZoomForResolution(view.getResolutionForExtent(seedExtent)));
    }
    const maxZoom = options.maxZoom == undefined ? 17 : options.maxZoom;
    const tileGrid = source.getTileGrid();
    const tileCoords = [];
    for (let zoom = minZoom; zoom <= maxZoom; ++zoom) {
      tileGrid.forEachTileCoord(seedExtent, zoom, tileCoord => {
        tileCoords.push(tileCoord);
      });
    }

    let i = 0, ii = tileCoords.length;
    console.log(`${this.name}: Seeding ${ii} tiles`); // eslint-disable-line
    let pctTime = Date.now();
    let etdTime = pctTime;
    let url = this.url;

    const files = {};
    let toStore = ii;
    let loading = 0;

    let loadMore = () => {
      while (loading < 100 && i < ii) {
        (i => {
          ++loading;
          const tileCoord = tileCoords[i];
          const src = url(tileCoord, this.pixelRatio, this.projection);
          fetch(src).then(response => {
            if (response.ok) {
              return this.source instanceof TileImage ? response.blob() : response.arrayBuffer();
            }
            throw new Error(`${src} with HTTP status ${response.status}`);
          }).then(data => {
            files[tileCoord] = data;
          }).catch(err => {
            --loading;
            --toStore;
          });
        })(i++);
      }
    };

    this.dbPromise().then(db => {

      this.emit('progress', {etd: 0, percent: 0, tiles: ii});
      let stored = 0;

      let storeMore = () => {
        let storing = {};
        let tx = db.transaction('tiles', 'readwrite');
        let tiles = tx.objectStore('tiles');
        tx.complete.then(() => {
          for (let k in storing) { //eslint-disable-line
            --loading;
            ++stored;
            if (stored % 100 === 0) {
              const time = Date.now();
              const etd = Math.round(((time - etdTime) / 100 * (toStore - stored)) / 1000 / 60);
              const percent = (Math.round(stored / toStore * 1000) / 10);
              etdTime = time;
              console.log(`${this.name}: ${percent}% complete. ETD: ${Math.floor(etd / 60)}h ${etd % 60}min`); //eslint-disable-line
              this.emit('progress', {etd, percent, tiles: toStore});
            }
          }
        }).catch(err => {
          console.log('repeating transaction due to failure'); //eslint-disable-line
          Object.assign(files, storing);
        });
        for (let key in files) {
          ((file, key) => {
            tiles.put(file, key);
            storing[key] = file;
            delete files[key];
          })(files[key], key);
        }
      };

      const timer = setInterval(() => {
        loadMore();
        if (stored == toStore) {
          clearInterval(timer);
          this.emit('progress', {etd: 0, percent: 100, tiles: toStore});
        }
        for (let key in files) { //eslint-disable-line
          storeMore();
          break;
        }
      }, 500);
    }).catch(err => {
      console.error('db error: ' + err.message); //eslint-disable-line
    });

  }
}
