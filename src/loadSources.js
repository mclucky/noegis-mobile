import GeoJSON from 'ol/format/GeoJSON';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import localforage from 'localforage';

export default class LoadSources {
  /**
   * initializes loading and handling saved geometries
  * @param {Object} options Options.
  * @param {module:ol/map} options.map OpenLayers map.
  */
  constructor(options) {
    this.map = options.map;
    this.selectList = document.createElement('select'); // select for layerNames
  }


  testModule() {
    return ('custom Module loadSources is working');
  }


  /**
   * create and populate layer source selection
   * @param {HTMLDivElement} container container for source selection.
   * @param {Array} layerNames Array of strings containing all store names (Layer Names).
   */
  initForm(container, layerNames) { // static member function, can be called without instantiating LoadSources Class
    this.selectList.id = 'layerSelection';
    for (let i = 0, len = layerNames.length; i < len; i++) {
      layerNames[i];
      let option = document.createElement('option');
      option.value = layerNames[i];
      option.text = layerNames[i];
      this.selectList.appendChild(option);
    }
    container.appendChild(this.selectList);
    this.loadLayer(layerNames[0], true); //show first Layer
  }


  /**
   * loads all vector features as a new layer to the map
   * @param {string} storeName Name of desired Store. One Store per Object Class
   * @param {bool} createNewLayer create new layer if true, put features into InitialStoreName-Layer if false
   */
  loadLayer(storeName, createNewLayer) {
    if (storeName === undefined) {
      return;
    }
    //remove old layer
    let oldLayer = this.map.getLayers().getArray()[1];
    if ((typeof oldLayer !== 'undefined') && (oldLayer.get('name') !== 'drawingLayer')) {
      this.map.removeLayer(oldLayer);
    }
    let layerStore = localforage.createInstance({
      name: 'LayersDataBase',
      storeName: storeName
    });
    let featureArray = [];
    let that = this; // other way?
    layerStore.iterate(function(value, key, iterationNumber) {
    // Resulting key/value pair -- this callback
      featureArray.push(value);
    }).then(function() {
      console.log('got all features from layerStore ', storeName);
      let vectorSource = that.featureArrayToVectorSource(featureArray);
      if(createNewLayer) {
        that.addLayerFromVectorSource(vectorSource);
      } else {
        console.log(that.map.getLayers().getArray());
        that.map.getLayers().getArray().forEach(function(layer){
          if (layer.get('name') === 'drawingLayer') {
            layer.getSource().addFeatures(vectorSource.getFeatures());
          }
        })
      }
      //return vectorSource;
    }).catch(function(err) {
      console.log(err);
    });
  }


  /**
   * returns an array of all store names in the DataBase
   * @param {array} featureArray array of geoJSON features
   */
  featureArrayToVectorSource(featureArray) {
    console.log('creating vector source...');
    let format = new GeoJSON;
    let parsedFeatureArray = [];
    featureArray.forEach(function(feature) {
      //parsedFeatureArray.push(JSON.parse(feature));
      parsedFeatureArray.push(format.readFeature(feature));
    })

    let vectorSource = new VectorSource({
      features: parsedFeatureArray
    });
    return (vectorSource);
  }

  /**
   * creates a layer from a given vector source and adds it to the map.
   * @param {module:ol/source/vector} vectorSource OpenLayers vector source.
   */
  addLayerFromVectorSource(vectorSource) {
    let newLayer = new VectorLayer({
      source: vectorSource
    })
    this.map.addLayer(newLayer);
    console.log('added new Layer');
    console.log(this.map.getLayers().getArray()[1].getSource());
  }

  /**
   * returns a Promise (resolves to an array) of all store names in the DataBase
   */
  getAllStoreNames() {
  // can not get all store names with localForage, compare issue:
  // https://github.com/localForage/localForage/issues/573
    return new Promise((resolve, reject) => {
      indexedDB.open('LayersDataBase').onsuccess = function(sender, args) {
        let obj = sender.target.result.objectStoreNames;
        let keys = [];
        for (let key in obj) {
          if (typeof obj[key] === 'string') {
            keys.push(obj[key]);
          }
        }
        for (var i=keys.length-1; i>=0; i--) {
          if (keys[i] === 'local-forage-detect-blob-support') { // store created by LocalForage, remove it from list
            keys.splice(i, 1);
            break;
          }
        }
        resolve(keys);
      };
    })
  }

  /**
   * creates example layers for testing
   * @param {number} amount amount of existing vector stores.
   */
  createRandomStores(amount) {
    console.log('creating random stores: ', 3-amount);
    let exampleFeature1 = '{"type":"Feature","id":"0","geometry":{"type":"Polygon","coordinates":[[[1709399.4820555388,5936099.616626788],[1719183.4216760413,5934532.657546941],[1719718.4808740376,5938889.5681591965],[1709399.4820555388,5936099.616626788]]]},"properties":null}';
    let exampleFeature2 = '{"type":"Feature","id":"0","geometry":{"type":"Polygon","coordinates":[[[1715896.6294597788,5950928.400114112],[1716737.4367709158,5947450.515327136],[1718443.3832240084,5950467.406345573],[1722505.5659851346,5949767.030007448],[1719143.7595621336,5953268.911698074],[1720264.361703134,5957611.24499445],[1716622.4047448828,5955370.04071245],[1713400.673589507,5957611.24499445],[1713820.899392382,5954389.513839074],[1711999.9209132565,5950747.556880823],[1715896.6294597788,5950928.400114112]]]},"properties":null}';
    let exampleFeature3 = '{"type":"Feature","id":"0","geometry":{"type":"Polygon","coordinates":[[[1709323.0450272537,5948405.978180701],[1707908.9600039779,5939539.282899621],[1710813.5670788146,5944354.815681587],[1710928.2226212425,5946074.648818003],[1711081.0966778127,5946839.019100855],[1711807.2484465218,5946915.45612914],[1709323.0450272537,5948405.978180701]]]},"properties":null}';
    let exampleFeatureArray = [exampleFeature1, exampleFeature2, exampleFeature3];

    let i = 0;
    while (i < (3-amount)) {
      let newLayerStore = localforage.createInstance({
        name: 'LayersDataBase',
        storeName: 'exampleLayer'+i
      });
      newLayerStore.setItem('0', exampleFeatureArray[i]).then(function(value) {
        console.log('added exampleStore: ', i);
        /*newLayerStore.dropInstance().then(function() {
          console.log('Dropped the store of the current instance');
        });*/
      }).catch(function(err) {
        console.log(err);
      });
      i++;
      if (i == (3 - amount)) {
        alert('Es wurden Beispiellayer zu Ihrem lokalen offline-Speicher hinzugefügt. Bitte laden Sie die Seite neu.');
      }
    }
  }

  /**
   * exports a Layer as GeoJSON
   * @param {string} storeName Name of desired Store. One Store per Object Class
   */
  exportLayer(storeName) {
    let writer = new GeoJSON();
    let vectorSource = this.map.getLayers().getArray()[1].getSource();
    let geojsonStr = writer.writeFeatures(vectorSource.getFeatures());
    const saveName = storeName + '.json';
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(new Blob([geojsonStr]), saveName);
    } else {
      let element = document.createElement('a');
      const dataURI = 'data:text/plain;charset=utf-8,' + encodeURIComponent(geojsonStr);
      element.setAttribute('href', dataURI);
      element.setAttribute('download', saveName);
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }
  }

  /**
   * imports a GeoJSON File as Object Store and refreshes select list of available layers
   * @param {object} file GeoJSON file
   */
  async importLayer(file) {
    console.log(file);
    console.log("name : " + file.name);
    let longFileName = file.name;
    let fileName = longFileName.split('.')[longFileName.split('.').length - 2];
    if (await this.askLayerOverride(fileName) === false) {
      console.log('no data was imported');
      return;
    }
    let reader = new FileReader();
    reader.onload = (function(e) {
      let data = JSON.parse(e.target.result);
      //create new IndexedDB Layer Store for importet LayersDataBase
      let newLayerStore = localforage.createInstance({
        name: 'LayersDataBase',
        storeName: fileName
      });

      // put features into new store
      let format = new GeoJSON;
      for (var i = 0; i < data.features.length; i++) {
        let featureString = JSON.stringify(data.features[i]);
        newLayerStore.setItem(data.features[i].id.toString(), featureString).then(function(value) {
        }).catch(function(err) {
          console.log(err);
        });
      }
      // add layer to selectList
      let newOption = document.createElement('option');
      newOption.value = fileName;
      newOption.text = fileName;
      this.selectList.appendChild(newOption);
      alert('file imported as layer: ' + fileName);
    }.bind(this))
    reader.readAsText(file);
  }

  /**
   * checks if layer about to be imported already exists in indexedDB, asks user for override if so (returns promise that resolves to bool)
   * @param {string} layerName name of layer (=filename without extension) to be imported
   */
  async askLayerOverride(layerName) {
    const storeNames = await this.getAllStoreNames();
    console.log(storeNames);
    let override = true;
    if (storeNames.indexOf(layerName) > -1) {
      override = confirm('The chosen layer already exists in the offline storage. Continue? (will override existing data of that layer)');
      console.log('user input = ', override);
    }
    return override;
  }
}
